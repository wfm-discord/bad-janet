// Description:
//   This script has been ported over to BJ bot

function moved(message) {
  message.reply('This command has been moved over to <@1078719671022395442>. Just perform the same command but tag the other bot.');
}

module.exports = function(robot) {
  robot.hear(/^corner audit/i, async function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * hubot slow opt out
   */
  robot.respond(/slow\s?opt\s?out/i, function() {
    moved(robot.discordJsMessage);
  });

  /**
   *hubot slow opt in
   */
  robot.respond(/slow\s?opt\s?in/i, function() {
    moved(robot.discordJsMessage);
  });

  /**
   * hubot my slowdown
   */
  robot.respond(/my\s?slow\s?down/i, async function (msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * Moved to BJ
   */
  robot.respond(/check\s?(on)?\s?@(.*)\s*grounding/i, async function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * Moved to BJ
   */
  robot.respond(/ground @/i, async function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * slowcorner @user (time)
   */
  robot.respond(/slow\s?corner\s+(@|\d+)/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * slow my corner (time)
   */
  robot.respond(/slow my corner/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * check slowdowns
   */
  robot.respond(/check slowdowns?$/i, async function(msg) {
    moved(robot.discordJsMessage);
  });

  /**
   * shame me
   */
  robot.respond(/shame me hard/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/shame me$/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/shame (on )?you/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/shame @/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/rename @/i, function(msg) {
    robot.discordJsMessage.reply('This command has been moved over to <@1078719671022395442>. It\'s a slash command now. Check out `/rename`');
  });

  robot.respond(/(add|override)\s?(one[-\s]?time )?count(ing)?\s?(message|msg) @/i, function (msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/remove\s?count(ing)?\s?(message|msg)s? /i, function (msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/see\s?count(ing)?\s?(message|msg)s?/i, function (msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/consent\?$/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/end consent$/i, function(msg) {
    moved(robot.discordJsMessage + '\n' +
      'Also, command not correctly formed. Did you forget to tag who you want to end consent to?');
  });

  robot.respond(/consent$/i, function(msg) {
    moved(robot.discordJsMessage + '\n' +
      'Also, command not correctly formed. Did you forget to tag who you want to consent to?');
    moved(robot.discordJsMessage);
  });

  robot.respond(/consent (to )?/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/end consent (to )?/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/(safeword|red|pineapple)/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/end safeword/i, function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/received consent/i, async function(msg) {
    moved(robot.discordJsMessage);
  });

  robot.respond(/given consent/i, async function(msg) {
    moved(robot.discordJsMessage);
  });
}
