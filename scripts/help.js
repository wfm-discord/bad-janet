// Description:
//    respond to help
//
// Commands:
//    hubot help

module.exports = function(robot) {
  robot.respond(/help/i, function(msg) {
    robot.discordJsMessage.reply("I'm not sure anyone can help you. I know I can't.");
  });
}
